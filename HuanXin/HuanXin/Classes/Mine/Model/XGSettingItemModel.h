//
//  XGSettingItemModel.h
//  HuanXin
//
//  Created by monkey on 2019/7/9.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XGSettingItemModel : NSObject

/// 标题
@property (nonatomic,copy) NSString *title;
/// 详细
@property (nonatomic,copy) NSString *detailText;
/// 监听方法
@property (nonatomic,copy) NSString *funcName;

@end

NS_ASSUME_NONNULL_END
