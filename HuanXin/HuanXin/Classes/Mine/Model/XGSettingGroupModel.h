//
//  XGSettingGroupModel.h
//  HuanXin
//
//  Created by monkey on 2019/7/9.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XGSettingItemModel;

NS_ASSUME_NONNULL_BEGIN

@interface XGSettingGroupModel : NSObject

/// 标签选项
@property (nonatomic,strong) NSArray<XGSettingItemModel *> *items;

@end

NS_ASSUME_NONNULL_END
