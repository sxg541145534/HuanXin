//
//  XGSettingGroupModel.m
//  HuanXin
//
//  Created by monkey on 2019/7/9.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <MJExtension/MJExtension.h>

#import "XGSettingGroupModel.h"
#import "XGSettingItemModel.h"

@implementation XGSettingGroupModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"items": [XGSettingItemModel class]};
}

@end
