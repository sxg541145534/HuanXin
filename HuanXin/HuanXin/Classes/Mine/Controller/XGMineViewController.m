//
//  XGMineViewController.m
//  HuanXin
//
//  Created by monkey on 2019/7/8.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>

#import "XGMineViewController.h"
#import "XGSettingTableViewController.h"

@interface XGMineViewController ()

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@end

@implementation XGMineViewController

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    // 设置第0组 间距为10 默认为35
    self.tableView.contentInset = UIEdgeInsetsMake(-25, 0, 0, 0);
    _userNameLabel.text = [EMClient sharedClient].currentUsername;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 3 && indexPath.row == 0) {
        XGSettingTableViewController *viewController = [[XGSettingTableViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

@end
