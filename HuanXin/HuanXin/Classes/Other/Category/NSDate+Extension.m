//
//  NSDate+Extension.m
//  TripWaterLayout
//
//  Created by monkey on 2019/11/22.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "NSDate+Extension.h"

static NSDateFormatter *_dateFormatter;

@implementation NSDate (Extension)

+ (void)initialize
{
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
}

- (NSString *)stringWithFormat:(NSString *)format
{
    _dateFormatter.dateFormat = format;
    return [_dateFormatter stringFromDate:self];
}

+ (NSString *)currentDateStringWithFormat:(NSString *)format
{
    return [[NSDate date] stringWithFormat:format];
}

- (NSString *)sinaDateDescription
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    if ([calendar isDateInToday:self]) {
        NSInteger seconds = (NSInteger)[[NSDate date] timeIntervalSinceDate:self];
        if (seconds < 60) {
            return @"刚刚";
        } else if (seconds < 3600) {
            return [NSString stringWithFormat:@"%zd分钟前",seconds / 60];
        } else {
            return [NSString stringWithFormat:@"%zd小时前",seconds / 3600];
        }
    }
    
    NSString *format;
    if ([calendar isDateInYesterday:self]) {
        format = @"昨天 HH:mm";
    } else if ([calendar components:NSCalendarUnitYear fromDate:self toDate:[NSDate date] options:0].year < 1) {
        format = @"MM-dd HH:mm";
    } else {
        format = @"yyyy-MM-dd HH:mm";
    }
    
    return [self stringWithFormat:format];
}

- (NSString *)weiChatDateDescription
{
    NSString *dateNow = [[NSDate date] stringWithFormat:@"yyyy/MM/dd HH:mm"];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:[[dateNow substringWithRange:NSMakeRange(8, 2)] intValue]];
    [components setMonth:[[dateNow substringWithRange:NSMakeRange(5, 2)] intValue]];
    [components setYear:[[dateNow substringWithRange:NSMakeRange(0, 4)] intValue]];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDate *date = [gregorian dateFromComponents:components]; // 今天凌晨时间
    
    NSTimeInterval ti = [self timeIntervalSinceDate:date];
    NSInteger hour = (NSInteger) (ti / 3600); // 计算与今天凌晨相差多少小时
    NSString *format = nil;
       
    // If hasAMPM == TURE, use 12-hour clock, otherwise use 24-hour clock
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
       
       if (!hasAMPM) { // 24-hour clock
           if (hour <= 24 && hour >= 0) {
               format = @"HH:mm";
           } else if (hour < 0 && hour >= -24) {
               format = @"昨天HH:mm";
           } else {
               format = @"yyyy/MM/dd HH:mm";
           }
       } else {
           if (hour >= 0 && hour <= 6) {
               format = @"凌晨hh:mm";
           } else if (hour > 6 && hour <= 11 ) {
               format = @"上午hh:mm";
           } else if (hour > 11 && hour <= 17) {
               format = @"下午hh:mm";
           } else if (hour > 17 && hour <= 24) {
               format = @"晚上hh:mm";
           } else if (hour < 0 && hour >= -24) {
               format = @"昨天HH:mm";
           } else {
               format = @"yyyy/MM/dd HH:mm";
           }
       }
       
    _dateFormatter.dateFormat = format;
    return [_dateFormatter stringFromDate:self];
}

+ (NSDate *)dateWithTimeIntervalInMilliSecondSince1970:(double)aMilliSecond
{
    double timeInterval = aMilliSecond;
    // judge if the argument is in secconds(for former data structure).
    if(aMilliSecond > 140000000000) {
        timeInterval = aMilliSecond / 1000;
    }
    
    return [NSDate dateWithTimeIntervalSince1970:timeInterval];
}

@end
