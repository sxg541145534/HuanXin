//
//  UIAlertController+Extension.h
//  HuanXin
//
//  Created by monkey on 2019/11/27.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Extension)

- (void)addAlertActionWithTitle:(NSString *)title action:(void (^)(UIAlertAction *action))actionBlock style:(UIAlertActionStyle)style;

@end

