//
//  UIColor+Extension.h
//  MeiTuanHD
//
//  Created by monkey on 2017/8/18.
//  Copyright © 2017年 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

/// RGB颜色
#define RGBColor(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

/// 十六进制的颜色转换为UIColor(RGB)
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/// 随机颜色
#define RandomColor [UIColor colorWithRed:arc4random_uniform(256) / 255.0 green:arc4random_uniform(256) / 255.0 blue:arc4random_uniform(256) / 255.0 alpha:1.f]

@interface UIColor (Extension)

/**
   颜色转换：iOS中（以#开头）十六进制的颜色转换为UIColor(RGB)

 @param color 十六进制的颜色
 @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)color;


/**
 RGB颜色

 @param red R
 @param green G
 @param blue B
 @param alpha alpha
 @return UIColor
 */
+ (UIColor *)red:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;

/** 随机颜色 */
+ (UIColor *)randomColor;


@end
