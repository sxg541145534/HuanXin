//
//  UIViewController+XG.h
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (XG)

/**
 弹框

 @param title 标题
 @param message 信息
 @param destructiveTitle 取消标题
 @param destructiveAction 取消事件
 @param defaultTitle 确定标题
 @param defaultAction 确定事件
 */
- (void)alertWithTitle:(NSString *_Nullable)title message:(NSString *_Nullable)message destructiveTitle:(NSString *_Nullable)destructiveTitle destructiveAction:(void(^_Nullable)(UIAlertAction * _Nullable action))destructiveAction defaultTitle:( NSString *_Nullable)defaultTitle defaultAction:(void(^ _Nullable)(UIAlertAction * _Nullable  action))defaultAction;

@end
