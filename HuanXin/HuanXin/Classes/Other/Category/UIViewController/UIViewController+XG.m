//
//  UIViewController+XG.m
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "UIViewController+XG.h"

#import <objc/message.h>

@implementation UIViewController (XG)

- (void)alertWithTitle:(NSString *_Nullable)title message:(NSString *_Nullable)message destructiveTitle:(NSString *_Nullable)destructiveTitle destructiveAction:(void(^_Nullable)(UIAlertAction * _Nullable action))destructiveAction defaultTitle:( NSString *_Nullable)defaultTitle defaultAction:(void(^ _Nullable)(UIAlertAction * _Nullable  action))defaultAction;

{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (destructiveTitle != nil) {
        [alert addAction:[UIAlertAction actionWithTitle:destructiveTitle style:UIAlertActionStyleDestructive handler:destructiveAction]];
    }
    
    if (defaultTitle != nil) {
        [alert addAction:[UIAlertAction actionWithTitle:defaultTitle style:UIAlertActionStyleDefault handler:defaultAction]];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}


+ (void)load
{
    Method present = class_getInstanceMethod(self, @selector(presentViewController:animated:completion:));
    Method xg_present = class_getInstanceMethod(self, @selector(xg_presentViewController:animated:completion:));
    method_exchangeImplementations(present, xg_present);
}

- (void)xg_presentViewController:(UIViewController *_Nonnull)viewControllerToPresent animated: (BOOL)flag completion:(void (^_Nullable)(void))completion
{
    viewControllerToPresent.modalPresentationStyle = UIModalPresentationFullScreen;
    [self xg_presentViewController:viewControllerToPresent animated:flag completion:completion];
}

@end
