//
//  NSString+Extension.h
//  TripWaterLayout
//
//  Created by monkey on 2019/11/20.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;
- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth maxHeight:(CGFloat)maxHeight;

+ (NSString *)stringFromNumber:(NSInteger)num;

@end

