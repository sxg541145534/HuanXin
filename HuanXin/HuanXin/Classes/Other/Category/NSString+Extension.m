//
//  NSString+Extension.m
//  TripWaterLayout
//
//  Created by monkey on 2019/11/20.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "NSString+Extension.h"


@implementation NSString (Extension)

- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth maxHeight:(CGFloat)maxHeight
{
    return [self boundingRectWithSize:CGSizeMake(maxWidth, maxHeight) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading  attributes:@{NSFontAttributeName: font} context:nil].size;
}

- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth
{
    return [self sizeWithFont:font maxWidth:maxWidth maxHeight:CGFLOAT_MAX];
}

+ (NSString *)stringFromNumber:(NSInteger)num
{
    NSString *str = nil;
    if (num < 10000) {
        str = [NSString stringWithFormat:@"%zd",num];
    } else {
        str = [NSString stringWithFormat:@"%.2f万",num / 10000.f];
        NSRange range = [str rangeOfString:@".00" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            str = [str stringByReplacingCharactersInRange:range withString:@""];
        } else  {
            range = [str rangeOfString:@"0" options:NSBackwardsSearch];
            if (range.location != NSNotFound) {
                str = [str stringByReplacingCharactersInRange:range withString:@""];
            }
        }
    }
    
    return str;
}

@end
