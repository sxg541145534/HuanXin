//
//  NSObject+Runtime.h
//  消息转发流程
//
//  Created by monkey on 2019/7/11.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (Runtime)

/// 获取成员变量列表
- (NSArray<NSString *> *)ivarList;
/// 获取属性列表
- (NSArray<NSString *> *)propertyList;

@end
