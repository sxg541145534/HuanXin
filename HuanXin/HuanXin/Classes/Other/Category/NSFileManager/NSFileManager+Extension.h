//
//  NSFileManager+Extension.h
//  BSBuDeJie
//
//  Created by monkey on 2018/12/18.
//  Copyright © 2018 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSFileManager (Extension)


/**
 获取指定路径文件大小

 @param filePath 文件路径
 @param completion 完成回调
 */
+ (void)getFileSize:(NSString *)filePath completion:(void (^)(NSUInteger fileSize))completion;


/**
 删除指定路径

 @param filePath 文件路径
 @param completion 完成回调
 */
+ (void)removeFilePath:(NSString *)filePath completion:(void (^)(NSError *_Nullable error))completion;

@end

NS_ASSUME_NONNULL_END
