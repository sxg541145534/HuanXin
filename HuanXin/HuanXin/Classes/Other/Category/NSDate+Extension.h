//
//  NSDate+Extension.h
//  TripWaterLayout
//
//  Created by monkey on 2019/11/22.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extension)

+ (NSString *)currentDateStringWithFormat:(NSString *)format;
- (NSString *)stringWithFormat:(NSString *)format;

- (NSString *)sinaDateDescription;
- (NSString *)weiChatDateDescription;

+ (NSDate *)dateWithTimeIntervalInMilliSecondSince1970:(double)aMilliSecond;

@end
