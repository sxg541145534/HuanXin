//
//  UIAlertController+Extension.m
//  HuanXin
//
//  Created by monkey on 2019/11/27.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "UIAlertController+Extension.h"

@implementation UIAlertController (Extension)

- (void)addAlertActionWithTitle:(NSString *)title action:(void (^)(UIAlertAction *action))actionBlock style:(UIAlertActionStyle)style;
{
    [self addAction:[UIAlertAction actionWithTitle:title style:style handler:actionBlock]];
}

@end
