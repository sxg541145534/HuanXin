//
//  NSObject+Runtime.m
//  消息转发流程
//
//  Created by monkey on 2019/7/11.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <objc/runtime.h>

#import "NSObject+Runtime.h"

@implementation NSObject (Runtime)

- (NSArray<NSString *> *)ivarList
{
    unsigned int count = 0;
    Ivar *ivar = class_copyIvarList([self class], &count);
    if (count == 0) {
        return @[];
    }
    
    NSMutableArray<NSString *> *arrM = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        const char *cName = ivar_getName(ivar[i]);
        NSString *name = [NSString stringWithUTF8String:cName];
        [arrM addObject:name];
    }
    
    free(ivar);
    return [arrM copy];
}

- (NSArray<NSString *> *)propertyList
{
    unsigned int count = 0;
    objc_property_t *propertyList = class_copyPropertyList([self class], &count);
    if (count == 0) {
        return @[];
    }
    
    NSMutableArray<NSString *> *arrM = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        const char *cName = property_getName(propertyList[i]);
        NSString *name = [NSString stringWithUTF8String:cName];
        [arrM addObject:name];
    }
    
    free(propertyList);
    return [arrM copy];
}

@end
