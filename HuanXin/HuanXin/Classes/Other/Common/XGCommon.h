//
//  XGCommon.h
//  HuanXin
//
//  Created by monkey on 2019/7/9.
//  Copyright © 2019 itcast. All rights reserved.
//

/// 切换根控制器通知
extern NSString *const XGSwitchApplicationRootViewControllerNotification;
/// 根控制键
extern NSString *const XGRootViewControllerClassNameKey;

@interface XGCommon : NSObject

+ (instancetype)shared;

/// 屏幕宽度
- (CGFloat)screenWidth;
/// 屏幕高度
- (CGFloat)screenHeight;
/// 屏幕比例
- (CGFloat)screenScale;
/// 状态栏高度
- (CGFloat)statusBarHeight;
/// 导航栏高度
- (CGFloat)navigationBarHeight;
/// tabBar高度
- (CGFloat)tabBarHeight;
/// 底部间距
- (CGFloat)bottomMargin;
/// 是否是iphoneX
- (BOOL)isIphoneX;
/// 是否可用ios11
- (BOOL)isIOS11;

@end

