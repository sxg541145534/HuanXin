//
//  XGRegisterViewController.h
//  HuanXin
//
//  Created by monkey on 2019/7/8.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XGRegisterViewController : UIViewController

@end

extern NSString * const XGRegisterSuccessNotificationName;
