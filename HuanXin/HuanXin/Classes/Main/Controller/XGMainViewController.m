//
//  XGMainViewController.m
//  HuanXin
//
//  Created by monkey on 2019/7/8.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>

#import "XGMainViewController.h"

@interface XGMainViewController () <EMContactManagerDelegate>

@end

@implementation XGMainViewController

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addAllChildViewControllers];
    [self setUpAppearance];
    
    [[EMClient sharedClient].contactManager addDelegate:self delegateQueue:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[EMClient sharedClient].contactManager removeDelegate:self];
}

- (void)dealloc
{
    DEBUG_Log_Method;
}

#pragma mark - 其他方法

/// 添加所有子控制器
- (void)addAllChildViewControllers
{
    [self addChildViewController:@"XGChatViewController" title:@"聊天" imageName:@"tabbar_mainframe" selectedImageName:@"tabbar_mainframeHL"];
    [self addChildViewController:@"XGContactsViewController" title:@"联系人" imageName:@"tabbar_contacts" selectedImageName:@"tabbar_contactsHL"];
    [self addChildViewController:@"XGDiscoverViewController" title:@"发现" imageName:@"tabbar_discover" selectedImageName:@"tabbar_discoverHL"];
    [self addChildViewController:@"XGMineViewController" title:@"我的" imageName:@"tabbar_me" selectedImageName:@"tabbar_meHL"];
}

- (void)addChildViewController:(NSString *)className title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    UIViewController *viewController = nil;
    if ([className isEqualToString:@"XGMineViewController"]) {
        viewController = [[UIStoryboard storyboardWithName:className bundle:nil] instantiateInitialViewController];
    } else {
        viewController = [[NSClassFromString(className) alloc] init];
    }
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    viewController.title = title;
    viewController.tabBarItem.image = [UIImage imageNamed:imageName];
    viewController.tabBarItem.selectedImage = [UIImage originalImageWithImageName:selectedImageName];
    [self addChildViewController:nav];
}

- (void)setUpAppearance
{
    UITabBar *tabBar = [UITabBar appearanceWhenContainedInInstancesOfClasses:@[[self class]]];
    tabBar.tintColor = [UIColor red:31 green:185 blue:34 alpha:1.f];
    
    UINavigationBar *navBar = [UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[self class]]];
    navBar.tintColor = [UIColor whiteColor];
    navBar.barTintColor = [UIColor red:41 green:43 blue:54 alpha:1.f];
    navBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    // 导航栏根控制器的状态栏样式 通过navBar.barStyle设置
    navBar.barStyle = UIBarStyleBlack;
}

#pragma mark - EMContactManagerDelegate

// 用户A发送加用户B为好友的申请，用户B同意后，用户A会收到这个回调
- (void)friendRequestDidApproveByUser:(NSString *)aUsername
{
    [self alertWithTitle:@"好友申请通知" message:[NSString stringWithFormat:@"用户%@同意了你的好友申请",aUsername] destructiveTitle:nil destructiveAction:nil defaultTitle:@"确定" defaultAction:^(UIAlertAction * _Nullable action) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"XGAddFriendRequestDidApproveNotification" object:nil];
    }];
}

// 用户A发送加用户B为好友的申请，用户B拒绝后，用户A会收到这个回调
- (void)friendRequestDidDeclineByUser:(NSString *)aUsername
{
    [self alertWithTitle:@"好友申请通知" message:[NSString stringWithFormat:@"用户%@拒绝了你的好友申请",aUsername] destructiveTitle:nil destructiveAction:nil defaultTitle:@"确定" defaultAction:nil];
}

@end
