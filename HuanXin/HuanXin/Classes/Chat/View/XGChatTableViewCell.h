//
//  XGConversationTableViewCell.h
//  HuanXin
//
//  Created by monkey on 2019/11/23.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGChatMessageModel;

@interface XGChatTableViewCell : UITableViewCell

/// 设置数据
/// @param messageModel 聊天信息模型
/// @param unreadCount 未读消息数
- (void)setMessageModel:(XGChatMessageModel *)messageModel unreadCount:(NSInteger)unreadCount;

@end

