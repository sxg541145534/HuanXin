//
//  XGVoiceRecoderView.m
//  HuanXin
//
//  Created by monkey on 2019/11/25.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGVoiceRecorderView.h"

@implementation XGVoiceRecorderView

#pragma mark - 构造方法

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI
{
    self.backgroundColor = [UIColor whiteColor];
    UIButton *button = [UIButton buttonWithTitle:@"按住录音" fontSize:17 titleColor:[UIColor darkGrayColor] imageName:@"chat_audio_blue" highlightedImageName:@"chat_audio_blue" target:nil action:nil];
    CGSize titleSize = [[button titleForState:UIControlStateNormal] sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}];
    CGFloat width = MAX(ceil(titleSize.width), button.imageView.image.size.width);
    button.bounds = CGRectMake(0, 0,width, button.imageView.image.size.height + 15 + ceil(titleSize.height));
    [button layoutButtonWithEdgeInsetsStyle:KButtonEdgeInsetsStyleTop imageTitleSpace:15];
    button.center = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height * 0.5);
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressButton:)];
    [button addGestureRecognizer:longPress];
    [self addSubview:button];
}

- (void)longPressButton:(UILongPressGestureRecognizer *)longPress
{
    UIButton *button = (UIButton *)longPress.view; // 录音按钮
    NSString *buttonTitle = nil; // 按钮标题
    NSString *imageName = nil;   // 按钮图片
    CGPoint point = [longPress locationInView:self];
    // 要在同一个坐标系中 才可以进行比较
    BOOL isContains = CGRectContainsPoint(longPress.view.frame, point);
    if (longPress.state == UIGestureRecognizerStateBegan) {
        buttonTitle = @"松开发送";
        if ([_delegate respondsToSelector:@selector(recorderVoiceButtonTouchDown:)]) {
            [_delegate recorderVoiceButtonTouchDown:self];
        }
    } else if (longPress.state == UIGestureRecognizerStateEnded) {
        // 按钮复原
        buttonTitle = @"按住录音";
        imageName = @"chat_audio_blue";
        if (isContains && [_delegate respondsToSelector:@selector(recorderVoiceButtonUpInside:)]) {
            // 按钮范围内松开
            [_delegate recorderVoiceButtonUpInside:self];
        } else if (!isContains && [_delegate respondsToSelector:@selector(recorderVoiceButtonTouchOutside:)]){
            // 按钮范围外松开
            [_delegate recorderVoiceButtonTouchOutside:self];
        }
    } else if (longPress.state == UIGestureRecognizerStateCancelled) {
        if ([_delegate respondsToSelector:@selector(recorderVoiceButtonCancelTouch:)]) {
            [_delegate recorderVoiceButtonCancelTouch:self];
        }
    } else if (longPress.state == UIGestureRecognizerStateChanged) {
        if (!isContains) {
            buttonTitle = @"松开取消";
            imageName = @"chat_audio_red";
        } else {
            buttonTitle = @"松开发送";
            imageName = @"chat_audio_blue";
        }
    }
    
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    if (imageName != nil) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
}
@end
