//
//  XGConversationTableViewCell.m
//  HuanXin
//
//  Created by monkey on 2019/11/23.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Masonry/Masonry.h>
#import <Hyphenate/Hyphenate.h>

#import "XGChatTableViewCell.h"

#import "XGChatMessageModel.h"


@interface XGChatTableViewCell ()

/// 头像
@property (nonatomic,strong) UIImageView *iconImageView;
/// 名称
@property (nonatomic,strong) UILabel *nameLabel;
/// 消息
@property (nonatomic,strong) UILabel *messageLabel;
/// 时间
@property (nonatomic,strong) UILabel *timeLabel;
/// 未读
@property (nonatomic,strong) UILabel *unreadLabel;

@end

@implementation XGChatTableViewCell

#pragma mark - 公开方法

- (void)setMessageModel:(XGChatMessageModel *)messageModel unreadCount:(NSInteger)unreadCount
{
    if ([messageModel.emmessage.from isEqualToString:[EMClient sharedClient].currentUsername]) {
        _nameLabel.text = messageModel.emmessage.to;
    } else {
        _nameLabel.text = messageModel.emmessage.from;
    }
    
    
    _messageLabel.text = messageModel.text;
    _timeLabel.text = messageModel.time;
    
    _unreadLabel.text = [NSString stringWithFormat:@"%zd",unreadCount];
    _unreadLabel.hidden = unreadCount == 0;
}

#pragma mark - 构造方法

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI
{
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.messageLabel];
    [self.contentView addSubview:self.unreadLabel];
    
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_iconImageView);
        make.left.equalTo(_iconImageView.mas_right).offset(15);
    }];
    
    [_messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_iconImageView);
        make.left.equalTo(_nameLabel);
    }];
    
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    [_unreadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-10);
        make.centerX.equalTo(_timeLabel);
        make.size.mas_equalTo(CGSizeMake(16, 16));
        
    }];
}

#pragma mark - 懒加载

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_avatar_blue"]];
    }
    
    return _iconImageView;
}

- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [UILabel labelWithText:@"测试" textColor:[UIColor darkGrayColor] font:14 textAlignment:NSTextAlignmentLeft];
    }
    
    return _nameLabel;
}

- (UILabel *)messageLabel
{
    if (!_messageLabel) {
        _messageLabel = [UILabel labelWithText:@"测试" textColor:[UIColor lightGrayColor] font:12 textAlignment:NSTextAlignmentLeft];
    }
    
    return _messageLabel;
}

- (UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [UILabel labelWithText:@"测试" textColor:[UIColor lightGrayColor] font:12 textAlignment:NSTextAlignmentLeft];
    }
    
    return _timeLabel;
}

- (UILabel *)unreadLabel
{
    if (!_unreadLabel) {
        _unreadLabel =  [UILabel labelWithText:@"10" textColor:[UIColor whiteColor] font:12 textAlignment:NSTextAlignmentCenter];
        _unreadLabel.backgroundColor = [UIColor redColor];
        _unreadLabel.layer.cornerRadius = 8;
        _unreadLabel.layer.masksToBounds = YES;
    }
    
    return _unreadLabel;
}
@end
