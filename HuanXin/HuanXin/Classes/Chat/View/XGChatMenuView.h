//
//  XGChatMenuView.h
//  HuanXin
//
//  Created by monkey on 2019/11/22.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol XGChatMenuViewDelegate;

@interface XGChatMenuView : UIView

/// 代理
@property (nonatomic,weak) id<XGChatMenuViewDelegate> delegate;

@end

@protocol XGChatMenuViewDelegate <NSObject>

- (void)chatMenuView:(XGChatMenuView *)chatMenuView sendMessage:(NSString *)message;

@end

