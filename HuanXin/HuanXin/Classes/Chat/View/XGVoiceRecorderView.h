//
//  XGVoiceRecoderView.h
//  HuanXin
//
//  Created by monkey on 2019/11/25.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol XGVoiceRecorderViewDelegate;

@interface XGVoiceRecorderView : UIView

/// 代理
@property (nonatomic,weak) id<XGVoiceRecorderViewDelegate> delegate;

@end

@protocol XGVoiceRecorderViewDelegate <NSObject>

@optional
- (void)recorderVoiceButtonTouchDown:(XGVoiceRecorderView *)voiceRecorderView;
- (void)recorderVoiceButtonUpInside:(XGVoiceRecorderView *)voiceRecorderView;
- (void)recorderVoiceButtonTouchOutside:(XGVoiceRecorderView *)voiceRecorderView;
- (void)recorderVoiceButtonCancelTouch:(XGVoiceRecorderView *)voiceRecorderView;

@end

