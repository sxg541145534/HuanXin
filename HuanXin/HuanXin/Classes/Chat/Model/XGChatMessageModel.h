//
//  XGChatMessageModel.h
//  HuanXin
//
//  Created by monkey on 2019/11/22.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessage.h>

@class EMMessage;

@interface XGChatMessageModel : JSQMessage

/// 信息模型
@property (nonatomic,strong) EMMessage *emmessage;
/// 时间
@property (nonatomic,readonly) NSString *time;

- (instancetype)initWithDate:(NSDate *)date emmessage:(EMMessage *)emmessage;
+ (instancetype)messageModelWithDate:(NSDate *)date emmessage:(EMMessage *)emmessage;

@end

