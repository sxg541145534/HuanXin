//
//  EMAudioPlayerHelper.h
//  ChatDemo-UI3.0
//
//  Created by XieYajie on 2019/1/29.
//  Copyright © 2019 XieYajie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGAudioPlayerHelper : NSObject

/// 单例
+ (instancetype)sharedHelper;

/// 开发播放音频
/// @param aPath 路径
/// @param aCompleton 完成回调
- (void)startPlayerWithPath:(NSString *)aPath
                 completion:(void(^)(NSError *error))aCompleton;

/// 停止播放
- (void)stopPlayer;

@end
