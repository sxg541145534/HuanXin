//
//  EMAudioRecordHelper.h
//  ChatDemo-UI3.0
//
//  Created by XieYajie on 2019/2/14.
//  Copyright © 2019 XieYajie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGAudioRecordHelper : NSObject

/// 单例
+ (instancetype)sharedHelper;

/// 开始录音
/// @param aPath 文件存放路径
/// @param aCompletion 完成回调
- (void)startRecordWithPath:(NSString *)aPath
                 completion:(void(^)(NSError *error))aCompletion;


/// 结束录音
/// @param aCompletion 完成回调
-(void)stopRecordWithCompletion:(void(^)(NSString *aPath, NSInteger aTimeLength))aCompletion;

/// 取消录音
-(void)cancelRecord;

@end
