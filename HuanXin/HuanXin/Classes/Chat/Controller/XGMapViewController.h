//
//  XGMapViewController.h
//  HuanXin
//
//  Created by monkey on 2019/11/25.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CLPlacemark;

@interface XGMapViewController : UIViewController

/// 构造方法
/// @param longitude 经度
/// @param latitude 纬度
- (instancetype)initWithLongitude:(CLLocationDegrees)longitude latitude:(CLLocationDegrees)latitude;

/// 设置发送位置完成回调
/// @param callBack 完成回调
- (void)setSendLocationCallBack:(void (^)(CLPlacemark *placemark))callBack;

@end
