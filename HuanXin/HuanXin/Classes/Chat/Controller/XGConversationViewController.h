//
//  XGDemoViewController.h
//  HuanXin
//
//  Created by monkey on 2019/11/22.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>

@interface XGConversationViewController : JSQMessagesViewController

- (instancetype)initWithUserName:(NSString *)userName;
+ (instancetype)conversationViewControllerWithUserName:(NSString *)userName;

/// 标记全部信息回调
@property (nonatomic,copy) void (^markAllCompletion)(NSString *conversationId);

@end

