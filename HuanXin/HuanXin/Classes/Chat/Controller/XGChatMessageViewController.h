//
//  XGChatMessageViewController.h
//  HuanXin
//
//  Created by monkey on 2019/11/22.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGUserModel;

@interface XGChatMessageViewController : UIViewController

- (instancetype)initWithUserModel:(XGUserModel *)userModel;

@end
