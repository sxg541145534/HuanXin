//
//  XGAddFriendTableViewCell.h
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGUserModel;

@interface XGAddFriendTableViewCell : UITableViewCell

/// 用户模型
@property (nonatomic,strong) XGUserModel *userModel;

@end
