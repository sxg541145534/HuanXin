//
//  XGItemTableViewCell.h
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGItemModel;

@interface XGItemTableViewCell : UITableViewCell

/// 数据模型
@property (nonatomic,strong) XGItemModel *itemModel;

@end

