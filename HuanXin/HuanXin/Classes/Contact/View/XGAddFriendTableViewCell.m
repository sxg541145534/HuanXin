//
//  XGAddFriendTableViewCell.m
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <Masonry/Masonry.h>

#import "XGAddFriendTableViewCell.h"

#import "XGUserModel.h"

@interface XGAddFriendTableViewCell ()

/// 用户头像
@property (nonatomic,strong) UIImageView *iconImageView;
/// 用户名
@property (nonatomic, strong) UILabel *userNameLabel;
/// 添加按钮
@property (nonatomic, strong) UIButton *addFriendButton;

@end

@implementation XGAddFriendTableViewCell

- (void)setUserModel:(XGUserModel *)userModel
{
    _userModel = userModel;
    
    _addFriendButton.enabled = true;
    _addFriendButton.backgroundColor = [UIColor orangeColor];
    _userNameLabel.text = userModel.userName;
}

#pragma mark - 事件监听

- (IBAction)addFriendAction:(UIButton *)button
{
    button.enabled = NO;
    button.backgroundColor = [UIColor lightGrayColor];
    
    [[EMClient sharedClient].contactManager addContact:_userModel.userName message:@"我想加您为好友" completion:^(NSString *aUsername, EMError *aError) {
        if (aError != nil) {
            [SVProgressHUD showErrorWithStatus:@"添加好友请求失败,该用户不存在!"];
            return;
        }
        
        [SVProgressHUD showSuccessWithStatus:@"请求已发送"];
    }];
}

#pragma mark - 构造方法

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI
{
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.userNameLabel];
    [self.contentView addSubview:self.addFriendButton];
    
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_addFriendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
        make.size.mas_equalTo(CGSizeMake(80, 36));
    }];
    
    [_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(_iconImageView.mas_right).offset(15);
        make.right.lessThanOrEqualTo(_addFriendButton.mas_left).offset(-15);
    }];
}

#pragma mark - 懒加载

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_avatar_blue"]];
    }
    
    return _iconImageView;
}

- (UILabel *)userNameLabel
{
    if (!_userNameLabel) {
        _userNameLabel = [UILabel labelWithText:@"菠萝吹雪" textColor:[UIColor darkGrayColor] font:16 textAlignment:NSTextAlignmentCenter];
    }
    
    return _userNameLabel;
}

- (UIButton *)addFriendButton
{
    if (!_addFriendButton) {
        _addFriendButton = [[UIButton alloc] init];
        _addFriendButton.backgroundColor = [UIColor orangeColor];
        [_addFriendButton setTitle:@"添加好友" forState:UIControlStateNormal];
        [_addFriendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_addFriendButton addTarget:self action:@selector(addFriendAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _addFriendButton;
}

@end
