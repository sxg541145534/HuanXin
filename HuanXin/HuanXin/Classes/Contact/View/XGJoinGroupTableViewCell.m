//
//  XGJoinGroupTableViewCell.m
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Masonry/Masonry.h>
#import <Hyphenate/EMGroup.h>

#import "XGJoinGroupTableViewCell.h"

#import "XGGroupModel.h"

@interface XGJoinGroupTableViewCell ()

/// 头像
@property (nonatomic,strong) UIImageView *iconImageView;
/// 标题
@property (nonatomic,strong) UILabel *nameLabel;
/// 加入按钮
@property (nonatomic,strong) UIButton *joinButton;

@end

@implementation XGJoinGroupTableViewCell

#pragma mark - 解析模型

- (void)setGroupModel:(XGGroupModel *)groupModel
{
    _groupModel = groupModel;
    
    _nameLabel.text = groupModel.group.subject;
    _joinButton.enabled = groupModel.enable;
    _joinButton.backgroundColor = groupModel.enable ? [UIColor red:36 green:157 blue:244 alpha:1] : [UIColor red:142 green:197 blue:252 alpha:1];
    [_joinButton setTitle:(groupModel.isJoined ? @"已加入" : @"已申请") forState:UIControlStateDisabled];
}

#pragma mark - 构造方法

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpUI
{
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.joinButton];
    
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
        
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(_iconImageView.mas_right).offset(15);
    }];
    
    [_joinButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
        make.size.mas_equalTo(CGSizeMake(60, 36));
    }];
}

#pragma mark - 事件监听

- (void)joinGroupAction
{
    _groupModel.enable = NO;
    _joinButton.enabled = NO;
    _joinButton.backgroundColor = [UIColor red:142 green:197 blue:252 alpha:1];
    if (_joinGroupCallBack) {
        _joinGroupCallBack(_groupModel.group.groupId);
        _joinGroupCallBack = nil;
    }
}

#pragma mark - 懒加载

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"group"]];
    }
    
    return _iconImageView;
}

- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [UILabel labelWithText:@"菠萝吹雪" textColor:[UIColor darkGrayColor] font:16 textAlignment:NSTextAlignmentCenter];
    }
    
    return _nameLabel;
}

- (UIButton *)joinButton
{
    if (!_joinButton) {
        _joinButton = [UIButton buttonWithTitle:@"加入" fontSize:15 titleColor:[UIColor whiteColor] target:self action:@selector(joinGroupAction)];
        _joinButton.backgroundColor = [UIColor red:36 green:157 blue:244 alpha:1];
        [_joinButton setTitle:@"已申请" forState:UIControlStateDisabled];
    }
    
    return _joinButton;
}
@end
