//
//  XGCheckTableViewCell.m
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Masonry/Masonry.h>

#import "XGCheckTableViewCell.h"

#import "XGItemModel.h"

@interface XGCheckTableViewCell ()

/// 头像
@property (nonatomic,strong) UIImageView *iconImageView;
/// 标题
@property (nonatomic,strong) UILabel *nameLabel;
/// 选中按钮
@property (nonatomic,strong) UIButton *checkerButton;

@end

@implementation XGCheckTableViewCell

#pragma mark - 解析模型

- (void)setItemModel:(XGItemModel *)itemModel
{
    _itemModel = itemModel;
    
    _iconImageView.image = [UIImage imageNamed:itemModel.iconName];
    _nameLabel.text = itemModel.title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    _checkerButton.selected = selected;
}

#pragma mark - 构造方法

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpUI
{
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.checkerButton];
    
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
        
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(_iconImageView.mas_right).offset(15);
    }];
    
    [_checkerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
}

#pragma mark - 懒加载

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
    }
    
    return _iconImageView;
}

- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [UILabel labelWithText:@"菠萝吹雪" textColor:[UIColor darkGrayColor] font:16 textAlignment:NSTextAlignmentCenter];
    }
    
    return _nameLabel;
}

- (UIButton *)checkerButton
{
    if (!_checkerButton) {
        _checkerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkerButton setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
        [_checkerButton setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
        _checkerButton.userInteractionEnabled = NO;
    }
    
    return _checkerButton;
}

@end
