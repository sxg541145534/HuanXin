//
//  XGJoinGroupTableViewCell.h
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGGroupModel;

@interface XGJoinGroupTableViewCell : UITableViewCell

/// 数据模型
@property (nonatomic,strong) XGGroupModel *groupModel;

/// 点击申请
@property (nonatomic,copy) void(^joinGroupCallBack)(NSString *groupId);

@end
