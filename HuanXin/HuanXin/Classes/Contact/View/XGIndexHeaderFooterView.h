//
//  XGIndexHeaderFooterView.h
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XGIndexHeaderFooterView : UITableViewHeaderFooterView

/// 标题
@property (nonatomic,copy) NSString *title;

@end

