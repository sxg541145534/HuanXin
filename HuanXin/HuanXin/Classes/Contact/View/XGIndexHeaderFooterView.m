//
//  XGIndexHeaderFooterView.m
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Masonry/Masonry.h>

#import "XGIndexHeaderFooterView.h"

@interface XGIndexHeaderFooterView ()

/// 标题
@property (nonatomic,strong) UILabel *titleLabel;

@end

@implementation XGIndexHeaderFooterView

#pragma mark - 公开方法

- (void)setTitle:(NSString *)title
{
    _title = [title copy];
    _titleLabel.text = _title;
}

#pragma mark - 构造方法

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI
{
    self.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    [self.contentView addSubview:self.titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(30);
    }];
}

#pragma mark - 懒加载

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel labelWithText:@"A" textColor:[UIColor darkGrayColor] font:18 textAlignment:NSTextAlignmentCenter];
    }
    
    return _titleLabel;
}

@end
