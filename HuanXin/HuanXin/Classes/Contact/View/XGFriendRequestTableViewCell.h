//
//  XGFriendRequestTableViewCell.h
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGFriendRequestNotification;

@interface XGFriendRequestTableViewCell : UITableViewCell

- (void)setFriendRequestNotification:(XGFriendRequestNotification *)notification solveHandle:(void (^)(XGFriendRequestNotification *notification,BOOL isApprove))solveHandle;

@end

