//
//  XGFriendRequestTableViewCell.m
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGFriendRequestTableViewCell.h"

#import "XGFriendRequestNotification.h"

@interface XGFriendRequestTableViewCell ()

/// 时间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
/// 用户名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
/// 申请信息
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
/// 处理block
@property (nonatomic,copy) void (^solveHandle)(XGFriendRequestNotification *, BOOL);
/// 请求通知
@property (nonatomic,strong) XGFriendRequestNotification *notification;

@end

@implementation XGFriendRequestTableViewCell

- (void)setFriendRequestNotification:(XGFriendRequestNotification *)notification solveHandle:(void (^)(XGFriendRequestNotification *, BOOL))solveHandle
{
    _timeLabel.text = notification.time;
    _nameLabel.text = notification.name;
    _messageLabel.text = notification.message.length > 0 ? notification.message : @"我想加您为好友";
    
    self.solveHandle = solveHandle;
    self.notification = notification;
}


- (IBAction)declineAction:(id)sender
{
    self.notification.solve = YES;
    self.solveHandle(self.notification, NO);
}

- (IBAction)approveAction:(id)sender
{
    self.notification.solve = YES;
    self.solveHandle(self.notification, YES);
}

@end
