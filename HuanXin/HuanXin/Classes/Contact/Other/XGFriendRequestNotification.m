//
//  XGAddFrientNotification.m
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGFriendRequestNotification.h"

@implementation XGFriendRequestNotification

- (instancetype)initWithCoder:(NSCoder *)coder
{
    if (self = [super init]) {
        _name = [[coder decodeObjectForKey:@"name"] copy];
        _message = [[coder decodeObjectForKey:@"message"] copy];
        _time = [[coder decodeObjectForKey:@"time"] copy];
        _solve = [coder decodeBoolForKey:@"solve"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_name forKey:@"name"];
    [coder encodeObject:_message forKey:@"message"];
    [coder encodeObject:_time forKey:@"time"];
    [coder encodeBool:_solve forKey:@"solve"];
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end
