//
//  XGAddFrientNotification.h
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGFriendRequestNotification : NSObject <NSSecureCoding>

/// 用户名
@property (nonatomic,copy) NSString *name;
/// 信息
@property (nonatomic,copy) NSString *message;
/// 时间
@property (nonatomic,copy) NSString *time;
/// 是否处理
@property (nonatomic,assign,getter=isSolve) BOOL solve;

@end

