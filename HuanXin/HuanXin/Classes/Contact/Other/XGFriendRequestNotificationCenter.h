//
//  XGContactNotificationCenter.h
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XGFriendRequestNotification;

@protocol XGContactNotificationCenterDelegate;

@interface XGFriendRequestNotificationCenter : NSObject

+ (instancetype)shared;

- (void)setDelegate:(id<XGContactNotificationCenterDelegate>)delegate;
- (void)removeDelegate:(id<XGContactNotificationCenterDelegate>)delegate;

- (NSArray<XGFriendRequestNotification *> *)notificationArr;
- (void)updateNotification;

@end

@protocol XGContactNotificationCenterDelegate <NSObject>

@optional
- (void)didReceiveFriendRequestNotification;

@end


