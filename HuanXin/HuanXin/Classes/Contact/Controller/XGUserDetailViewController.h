//
//  XGUserDetailViewController.h
//  HuanXin
//
//  Created by monkey on 2019/11/21.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGUserModel;

@interface XGUserDetailViewController : UITableViewController

+ (instancetype)userDetailViewController;

/// 用户模型
@property (nonatomic,strong) XGUserModel *userModel;

@end
