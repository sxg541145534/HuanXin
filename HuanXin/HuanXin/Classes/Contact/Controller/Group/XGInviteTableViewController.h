//
//  XGInviteTableViewController.h
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XGInviteTableViewController : UITableViewController

- (instancetype)initWithCompletionHandle:(void(^)(NSArray<NSString *> *userList))completionHandle;
+ (instancetype)inviteTableViewControllerWithCompletionHandle:(void(^)(NSArray<NSString *> *userList))completionHandle;

@end
