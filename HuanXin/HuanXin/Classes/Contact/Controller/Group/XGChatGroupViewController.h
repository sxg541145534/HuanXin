//
//  XGChatGroupViewController.h
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGGroupTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface XGChatGroupViewController : XGGroupTableViewController

@end

NS_ASSUME_NONNULL_END
