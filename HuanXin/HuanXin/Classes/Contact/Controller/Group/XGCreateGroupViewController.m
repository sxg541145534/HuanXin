//
//  XGCreateGroupViewController.m
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UITextView_Placeholder/UITextView+Placeholder.h>
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <Hyphenate/Hyphenate.h>

#import "XGCreateGroupViewController.h"

@interface XGCreateGroupViewController ()

/// 群名称
@property (nonatomic,strong) UITextField *nameTextField;
/// 群描述
@property (nonatomic,strong) UITextView *descriptionTextView;
/// 待邀请的用户数组
@property (nonatomic,strong) NSArray<NSString *> *userList;

@end

@implementation XGCreateGroupViewController

#pragma mark - 控制器生命周期方法

- (void)loadView
{
    [super loadView];
    
    [self setUpUI];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"创建群组";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"创建" style:UIBarButtonItemStylePlain target:self action:@selector(createGroupAction)];
}

#pragma mark - 构造方法

- (instancetype)initWithInviteUsers:(NSArray<NSString *> *)userList
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        _userList = userList;
    }
    
    return self;
}

+ (instancetype)createGroupViewControllerWithInviteUsers:(NSArray<NSString *> *)userList
{
    return [[self alloc] initWithInviteUsers:userList];
}

#pragma mark - 事件监听

- (void)createGroupAction
{
    if (_nameTextField.hasText && _descriptionTextView.hasText) {
        EMError *error = nil;
        EMGroupOptions *setting = [[EMGroupOptions alloc] init];
        setting.IsInviteNeedConfirm = NO; //邀请群成员时，是否需要发送邀请通知.若NO，被邀请的人自动加入群组
        setting.style = EMGroupStylePublicOpenJoin;// 创建不同类型的群组，这里需要才传入不同的类型
        [[EMClient sharedClient].groupManager createGroupWithSubject:_nameTextField.text description:_descriptionTextView.text invitees:_userList message:@"邀请您加入群组" setting:setting completion:^(EMGroup *aGroup, EMError *aError) {
            if(!error){
                [SVProgressHUD showSuccessWithStatus:@"群组创建成功"];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                DEBUG_Log(@"群组创建失败!");
            }
        }];
    } else {
        [SVProgressHUD showErrorWithStatus:@"群名称或群描述不能为空!"];
    }
}

#pragma mark - 其他方法

- (void)setUpUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.nameTextField];
    [self.view addSubview:self.descriptionTextView];
    
    [_nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(44);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
    }];
    
    [_descriptionTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameTextField.mas_bottom).offset(30);
        make.left.right.equalTo(_nameTextField);
        make.height.mas_equalTo(300);
    }];
}


#pragma mark - 懒加载

- (UITextField *)nameTextField
{
    if (!_nameTextField) {
        _nameTextField = [[UITextField alloc] init];
        _nameTextField.placeholder = @"请输入群名称";
        _nameTextField.borderStyle = UITextBorderStyleRoundedRect;
        _nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    
    return _nameTextField;
}

- (UITextView *)descriptionTextView
{
    if (!_descriptionTextView) {
        _descriptionTextView = [[UITextView alloc] init];
        _descriptionTextView.font = [UIFont systemFontOfSize:15];
        _descriptionTextView.placeholder = @"请输入群描述";
        _descriptionTextView.layer.borderWidth = 0.5;
        _descriptionTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
    return _descriptionTextView;
}

@end
