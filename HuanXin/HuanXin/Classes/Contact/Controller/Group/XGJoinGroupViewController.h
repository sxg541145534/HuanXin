//
//  XGJoinGroupViewController.h
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EMGroup;

@interface XGJoinGroupViewController : UITableViewController

/// 完成回调
@property (nonatomic,copy) void(^joinedCallBack)(BOOL isJoinGroup);

@end


