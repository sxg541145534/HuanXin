//
//  XGInviteTableViewController.m
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>

#import "XGInviteTableViewController.h"

#import "XGCheckTableViewCell.h"

#import "XGItemModel.h"

@interface XGInviteTableViewController ()
{
    /// 联系人数组
    NSArray<XGItemModel *> *_contactsArr;
    /// 完成回调
    void (^_completionHandle)(NSArray<NSString *> *);
}

@end

@implementation XGInviteTableViewController

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"选择群组成员";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_gray"] style:UIBarButtonItemStylePlain target:self action:@selector(goBackAction)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self setUpTableView];
    [self loadContacts];
}

- (void)dealloc
{
    DEBUG_Log_Method;
}

#pragma mark - 构造方法

- (instancetype)initWithCompletionHandle:(void (^)(NSArray<NSString *> *))completionHandle
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        _completionHandle = [completionHandle copy];
    }
    
    return self;
}

+ (instancetype)inviteTableViewControllerWithCompletionHandle:(void (^)(NSArray<NSString *> *))completionHandle
{
    return [[self alloc] initWithCompletionHandle:completionHandle];
}

#pragma mark - 事件监听

- (void)goBackAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneAction
{
    NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
    NSMutableArray<NSString *> *userArrM = [NSMutableArray arrayWithCapacity:selectedRows.count];
    for (NSIndexPath *index in selectedRows) {
        [userArrM addObject:_contactsArr[index.row].title];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (self->_completionHandle) {
            self->_completionHandle(userArrM);
        }
    }];
}

#pragma mark - 其他方法

- (void)loadContacts
{
    NSArray<NSString *> *userNameArr = [[EMClient sharedClient].contactManager getContacts];
    if (userNameArr.count > 0) {
        _contactsArr = [self contactsFromUserList:userNameArr];
    } else {
        EMError *error = nil;
        userNameArr = [[EMClient sharedClient].contactManager getContactsFromServerWithError:&error];
        if (error != nil || userNameArr.count == 0) {
            DEBUG_Log(@"服务器获取好友失败");
        } else {
            _contactsArr = [self contactsFromUserList:userNameArr];
        }
    }
}

- (NSArray<XGItemModel *> *)contactsFromUserList:(NSArray<NSString *> *)userList
{
    NSMutableArray<XGItemModel *> *tmpArrM = [NSMutableArray arrayWithCapacity:userList.count];
    for (NSString *name in userList) {
        [tmpArrM addObject:[XGItemModel itemModelWithTitle:name iconName:@"user_avatar_blue"]];
    }
    [tmpArrM sortUsingComparator:^NSComparisonResult(XGItemModel *obj1, XGItemModel *obj2) {
        return [obj1.title compare:obj2.title];
    }];
    return tmpArrM;
}

- (void)setUpTableView
{
    self.tableView.rowHeight = 64.0;
    self.tableView.allowsMultipleSelection = YES;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerClass:[XGCheckTableViewCell class] forCellReuseIdentifier:NSStringFromClass([XGCheckTableViewCell class])];
}

#pragma mark - 数据源和代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _contactsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XGCheckTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([XGCheckTableViewCell class])];
    cell.itemModel = _contactsArr[indexPath.item];
    return cell;
}
@end
