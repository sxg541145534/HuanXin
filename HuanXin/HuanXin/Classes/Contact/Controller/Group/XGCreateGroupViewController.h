//
//  XGCreateGroupViewController.h
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XGCreateGroupViewController : UIViewController

- (instancetype)initWithInviteUsers:(NSArray<NSString *> *)userList;
+ (instancetype)createGroupViewControllerWithInviteUsers:(NSArray<NSString *> *)userList;

@end

