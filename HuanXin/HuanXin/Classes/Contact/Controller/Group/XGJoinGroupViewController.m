//
//  XGJoinGroupViewController.m
//  HuanXin
//
//  Created by monkey on 2019/12/2.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "XGJoinGroupViewController.h"

#import "XGJoinGroupTableViewCell.h"

#import "XGGroupModel.h"

@interface XGJoinGroupViewController ()
{
    /// 聊天群组数组
    NSMutableArray<XGGroupModel *> *_chatGroupArr;
    /// 已加入的群组
    NSArray<XGGroupModel *> *_joinedGroupArr;
    BOOL _isJoined;
}

@end

@implementation XGJoinGroupViewController

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"加入群组";
    
    self.tableView.rowHeight = 64.0;
    [self.tableView registerClass:[XGJoinGroupTableViewCell class] forCellReuseIdentifier:NSStringFromClass([XGJoinGroupTableViewCell class])];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self loadJoinedGroups];
    [self loadAllPublicGroups];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_joinedCallBack) {
        _joinedCallBack(_isJoined);
        _joinedCallBack = nil;
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _chatGroupArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak typeof(self) weakSelf = self;
    XGJoinGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([XGJoinGroupTableViewCell class])];
    cell.groupModel = _chatGroupArr[indexPath.row];
    cell.joinGroupCallBack = ^(NSString *groupId) {
        [[EMClient sharedClient].groupManager joinPublicGroup:groupId completion:^(EMGroup *aGroup, EMError *aError) {
            if (aError) {
                DEBUG_Log(@"加入群组失败");
                return;
            }
            
            [SVProgressHUD showSuccessWithStatus:@"加入群组成功"];
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf->_isJoined = YES;
        }];
    };
    return cell;
}

#pragma mark - 其他方法

- (void)loadJoinedGroups
{
    NSMutableArray<XGGroupModel *> *tmpArrM = [NSMutableArray arrayWithCapacity:10];
    NSArray<EMGroup *> *groupArr = [[EMClient sharedClient].groupManager getJoinedGroups];
    if (groupArr.count > 0) {
        for (EMGroup *group in groupArr) {
            [tmpArrM addObject:[XGGroupModel groupModelWithGroup:group]];
        }
        _joinedGroupArr = tmpArrM;
    } else {
        [[EMClient sharedClient].groupManager getJoinedGroupsFromServerWithPage:1 pageSize:-1 completion:^(NSArray *aList, EMError *aError) {
            if (aError) {
                DEBUG_Log(@"获取用户加入的群组失败");
                return;
            }
            
            for (EMGroup *group in aList) {
                [tmpArrM addObject:[XGGroupModel groupModelWithGroup:group]];
            }
            self->_joinedGroupArr = tmpArrM;
        }];
    }
}

- (void)loadAllPublicGroups
{
    _chatGroupArr = [NSMutableArray arrayWithCapacity:20];
    [[EMClient sharedClient].groupManager getPublicGroupsFromServerWithCursor:nil pageSize:20 completion:^(EMCursorResult *aResult, EMError *aError) {
        if (aError || aResult.list.count == 0) {
            DEBUG_Log(@"获取公开群组失败");
            return;
        }
        
        for (EMGroup *group in aResult.list) {
            XGGroupModel *groupModel = [XGGroupModel groupModelWithGroup:group];
            groupModel.joined = [self containsGroup:groupModel];
            groupModel.enable = !groupModel.isJoined;
            [self->_chatGroupArr addObject:groupModel];
        }
        
        [self.tableView reloadData];
    }];
}

- (BOOL)containsGroup:(XGGroupModel *)groupModel
{
    for (XGGroupModel *joinedGroup in _joinedGroupArr) {
        if ([joinedGroup.group.groupId isEqualToString:groupModel.group.groupId]) {
            return YES;
        }
    }
    
    return NO;
}

@end
