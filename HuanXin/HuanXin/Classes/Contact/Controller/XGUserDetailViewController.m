//
//  XGUserDetailViewController.m
//  HuanXin
//
//  Created by monkey on 2019/11/21.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGUserDetailViewController.h"
#import "XGChatMessageViewController.h"
#import "XGConversationViewController.h"

#import "XGUserModel.h"

@interface XGUserDetailViewController ()

/// 用户昵称
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@end

@implementation XGUserDetailViewController

+ (instancetype)userDetailViewController
{
    return [[UIStoryboard storyboardWithName:NSStringFromClass(self) bundle:nil] instantiateInitialViewController];
}

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"联系人详情";
    _userNameLabel.text = self.userModel.userName;
    
    // 设置第0组 间距为10 默认为35
    self.tableView.contentInset = UIEdgeInsetsMake(-25, 0, 0, 0);
}

#pragma mark - tableView代理方法

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 3) {
        switch (indexPath.row) {
            case 0:
                [self chatMessage];
                break;
            case 1:
                NSLog(@"音视频通话");
                break;
        }
    }
}

#pragma mark - 其他方法

- (void)chatMessage
{
    XGConversationViewController *conversationViewController = [XGConversationViewController conversationViewControllerWithUserName:_userModel.userName];
    [self.navigationController pushViewController:conversationViewController animated:YES];
}
@end
