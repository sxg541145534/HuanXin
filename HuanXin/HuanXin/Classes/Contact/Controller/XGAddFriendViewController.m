//
//  XGAddFriendViewController.m
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Masonry/Masonry.h>

#import "XGAddFriendViewController.h"

#import "XGAddFriendTableViewCell.h"

#import "XGUserModel.h"

static NSString *kReuseIdentifier = @"XGAddFriendTableViewCell";

@interface XGAddFriendViewController () <UISearchBarDelegate,UITableViewDataSource>

/// 搜索框
@property (nonatomic,strong) UISearchBar *searchBar;
/// 结果列表
@property (nonatomic,strong) UITableView *tableView;
/// 用户列表
@property (nonatomic,strong) NSArray<XGUserModel *> *searchResultArr;

@end

@implementation XGAddFriendViewController

#pragma mark - 控制器生命周期方法

- (void)loadView
{
    [super loadView];
    
    [self setUpUI];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"添加好友";
}

- (void)dealloc
{
    DEBUG_Log_Method;
}

#pragma mark - UISearchBarDelegate

// 搜索框开始编辑
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

// 搜索框文字改变
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // 有文字的时候展示取消按钮
    BOOL showCancelButton = !(searchText == nil || [searchText isEqualToString:@""]);
    searchBar.showsCancelButton = showCancelButton;
    if (!showCancelButton) {
        _tableView.hidden = YES;
    }
}

// 搜索按钮事件
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    // 模拟搜索 应该发送网络请求
    _searchResultArr = @[[[XGUserModel alloc] initWithUserName:searchBar.text]];
    _tableView.hidden = NO;
    _searchBar.showsCancelButton = NO;
    [_tableView reloadData];
}

// 取消按钮事件
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    _searchResultArr = @[];
    _tableView.hidden = YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _searchResultArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XGAddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifier];
    cell.userModel = _searchResultArr[indexPath.row];
    return cell;
}


#pragma mark - 其他方法

- (void)setUpUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.tableView];
    
    [_searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
    }];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchBar.mas_bottom);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
    }];
}


#pragma mark - 懒加载

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.placeholder = @"请输入好友id";
        _searchBar.delegate = self;
    }
    
    return _searchBar;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.rowHeight = 64.f;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.hidden = YES;
        _tableView.allowsSelection = NO;
        _tableView.separatorInset = UIEdgeInsetsZero;
        [_tableView registerClass:[XGAddFriendTableViewCell class] forCellReuseIdentifier:kReuseIdentifier];
    }
    
    return _tableView;
}

@end
