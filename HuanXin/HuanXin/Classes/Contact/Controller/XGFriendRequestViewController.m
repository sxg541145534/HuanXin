//
//  XGFriendRequestTableViewController.m
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>

#import "XGFriendRequestViewController.h"
#import "XGFriendRequestNotificationCenter.h"

#import "XGFriendRequestTableViewCell.h"

#import "XGFriendRequestNotification.h"

/// 重用标识符
static NSString *kXGFriendRequestTableViewCellReuseIdentifier = @"XGFriendRequestTableViewCell";

@interface XGFriendRequestViewController ()

/// 信息数组
@property (nonatomic,strong) NSMutableArray<XGFriendRequestNotification *> *notificationArrM;
/// 是否添加好友
@property (nonatomic,assign,getter=isAddFriend) BOOL addFriend;

@end

@implementation XGFriendRequestViewController

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(goBackAction)];
    self.navigationItem.title = @"申请与通知";
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = 140;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([XGFriendRequestTableViewCell class]) bundle:nil] forCellReuseIdentifier:kXGFriendRequestTableViewCellReuseIdentifier];
}

- (void)dealloc
{
    DEBUG_Log_Method;
}

#pragma mark - 事件监听

- (void)goBackAction
{
    // 更新通知信息
    [[XGFriendRequestNotificationCenter shared] updateNotification];
    if (self.isAddFriend && _solveRequestCallBack) {
        _solveRequestCallBack(self.isAddFriend);
        _solveRequestCallBack = nil;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notificationArrM.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XGFriendRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kXGFriendRequestTableViewCellReuseIdentifier];
    __weak typeof(self) weakSelf = self;
    [cell setFriendRequestNotification:_notificationArrM[indexPath.row] solveHandle:^(XGFriendRequestNotification *notification, BOOL isApprove) {
        // 删除并更新通知
        [weakSelf.notificationArrM removeObject:notification];
        [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        weakSelf.addFriend = isApprove;
        if (isApprove) {
            // 用户同意
            [[EMClient sharedClient].contactManager approveFriendRequestFromUser:notification.name completion:nil];
        } else {
            // 用户拒绝
            [[EMClient sharedClient].contactManager declineInvitationForUsername:notification.name];
        }
    }];
    return cell;
}

#pragma mark - 懒加载

- (NSMutableArray<XGFriendRequestNotification *> *)notificationArrM
{
    if (!_notificationArrM) {
        _notificationArrM = [[[XGFriendRequestNotificationCenter shared] notificationArr] mutableCopy];
    }
    
    return _notificationArrM;
}

@end
