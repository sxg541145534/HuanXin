//
//  XGFriendRequestTableViewController.h
//  HuanXin
//
//  Created by monkey on 2019/11/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XGFriendRequestViewController : UITableViewController

/// 处理好友请求回调
@property (nonatomic,copy) void(^solveRequestCallBack)(BOOL isAccept);

@end

