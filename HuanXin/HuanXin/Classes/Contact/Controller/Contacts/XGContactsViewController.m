//
//  XGContactsViewController.m
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Hyphenate/Hyphenate.h>

#import "XGContactsViewController.h"
#import "XGAddFriendViewController.h"
#import "XGFriendRequestViewController.h"
#import "XGUserDetailViewController.h"
#import "XGChatGroupViewController.h"

#import "XGItemModel.h"
#import "XGUserModel.h"

#import "XGFriendRequestNotificationCenter.h"

@interface XGContactsViewController () <XGContactNotificationCenterDelegate>

@end

@implementation XGContactsViewController

#pragma mark - 控制器生命周期方法

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[XGFriendRequestNotificationCenter shared] setDelegate:self];
}

- (void)dealloc
{
    [[XGFriendRequestNotificationCenter shared] removeDelegate:self];
}

#pragma mark - 重写方法

- (NSArray<XGItemModel *> *)headerItems
{
    __weak typeof(self) weakSelf = self;
    
    XGItemModel *item1 = [XGItemModel itemModelWithTitle:@"添加好友" iconName:@"contact"];
    item1.selectedCallBlock = ^(NSString *title) {
        XGAddFriendViewController *viewController = [[XGAddFriendViewController alloc] init];
        [weakSelf.navigationController pushViewController:viewController animated:YES];
    };
    
    XGItemModel *item2 = [XGItemModel itemModelWithTitle:@"申请与通知" iconName:@"notification"];
    item2.selectedCallBlock = ^(NSString *title) {
        XGFriendRequestViewController *viewController = [[XGFriendRequestViewController alloc] init];
        viewController.solveRequestCallBack = ^(BOOL isAccept) {
            NSInteger bageValue = [XGFriendRequestNotificationCenter shared].notificationArr.count;
            weakSelf.tabBarItem.badgeValue = bageValue > 0 ? [NSString stringWithFormat:@"%zd",bageValue] : nil;
            if (isAccept) {
                [weakSelf reloadTableView];
            }
        };
        [weakSelf.navigationController pushViewController:viewController animated:YES];
    };

    XGItemModel *item3 = [XGItemModel itemModelWithTitle:@"群聊" iconName:@"group"];
    item3.selectedCallBlock = ^(NSString *title) {
        XGChatGroupViewController *chatGroupViewController = [[XGChatGroupViewController alloc] init];
        [weakSelf.navigationController pushViewController:chatGroupViewController animated:YES];
    };

    XGItemModel *item4 = [XGItemModel itemModelWithTitle:@"聊天室" iconName:@"chatroom"];
    
    XGItemModel *item5 = [XGItemModel itemModelWithTitle:@"多人视频" iconName:@"call"];
    
    return @[item1,item2,item3,item4,item5];
}

- (NSArray<XGItemModel *> *)contentItems
{
    NSArray<NSString *> *userNameArr = [[EMClient sharedClient].contactManager getContacts];
    if (userNameArr.count > 0) {
        return [self itemModelsWithTitles:userNameArr];
    } else {
        EMError *error = nil;
        userNameArr = [[EMClient sharedClient].contactManager getContactsFromServerWithError:&error];
        if (error != nil || userNameArr.count == 0) {
            DEBUG_Log(@"服务器获取好友失败");
            return @[];
        } else {
            return [self itemModelsWithTitles:userNameArr];
        }
    }
}

- (void)leftSlideDeleteAction:(NSString *)name completion:(void(^)(void))completion
{
    [[EMClient sharedClient].contactManager deleteContact:name isDeleteConversation:YES completion:^(NSString *aUsername, EMError *aError) {
        if (aError) {
            DEBUG_Log(@"删除好友失败");
            return;
        }

        completion();
    }];
}
#pragma mark - XGContactNotificationCenterDelegate

- (void)didReceiveFriendRequestNotification
{
    NSInteger bageValue = [XGFriendRequestNotificationCenter shared].notificationArr.count;
    self.tabBarItem.badgeValue = bageValue > 0 ? [NSString stringWithFormat:@"%zd",bageValue] : nil;
}

#pragma mark - 其他方法

- (NSArray<XGItemModel *> *)itemModelsWithTitles:(NSArray<NSString *> *)titlesArr
{
    __weak typeof(self) weakSelf = self;
    NSMutableArray<XGItemModel *> *tmp = [NSMutableArray arrayWithCapacity:titlesArr.count];
    for (NSString *title in titlesArr) {
        XGUserDetailViewController *detailViewController = [XGUserDetailViewController userDetailViewController];
        detailViewController.userModel = [XGUserModel userModelWithUserName:title];
        XGItemModel *itemModel = [XGItemModel itemModelWithTitle:title iconName:@"user_avatar_blue"];
        itemModel.selectedCallBlock = ^(NSString *title) {
            [weakSelf.navigationController pushViewController:detailViewController animated:YES];
        };
        [tmp addObject:itemModel];
    }
    
    return tmp;
}

@end
