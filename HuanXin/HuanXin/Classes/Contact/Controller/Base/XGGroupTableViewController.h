//
//  XGGroupTableViewController.h
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XGItemModel;

@interface XGGroupTableViewController : UITableViewController

/// 头部数据
- (NSArray<XGItemModel *> *)headerItems;

/// 内容数据
- (NSArray<XGItemModel *> *)contentItems;

/// 刷新tableView
- (void)reloadTableView;

/// 左滑删除事件
/// @param name 要删除的名称
/// @param completion 完成回调
- (void)leftSlideDeleteAction:(NSString *)name completion:(void(^)(void))completion;

@end
