//
//  XGItemModel.h
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGItemModel : NSObject

/// 名称
@property (nonatomic,copy) NSString *title;
/// 图标
@property (nonatomic,strong) NSString *iconName;
/// 事件
@property (nonatomic,copy) void(^selectedCallBlock)(NSString *title);

- (instancetype)initWithTitle:(NSString *)title iconName:(NSString *)iconName;
+ (instancetype)itemModelWithTitle:(NSString *)title iconName:(NSString *)iconName;

@end
