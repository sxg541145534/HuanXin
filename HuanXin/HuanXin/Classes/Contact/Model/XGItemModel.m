//
//  XGItemModel.m
//  HuanXin
//
//  Created by monkey on 2019/12/1.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGItemModel.h"

@implementation XGItemModel

- (instancetype)initWithTitle:(NSString *)title iconName:(NSString *)iconName
{
    if (self = [super init]) {
        _title = [title copy];
        _iconName = [iconName copy];
    }
    
    return self;
}

+ (instancetype)itemModelWithTitle:(NSString *)title iconName:(NSString *)iconName
{
    return [[self alloc] initWithTitle:title iconName:iconName];
}

@end
