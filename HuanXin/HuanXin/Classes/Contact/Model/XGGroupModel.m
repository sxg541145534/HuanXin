//
//  XGGroupModel.m
//  HuanXin
//
//  Created by monkey on 2019/12/3.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGGroupModel.h"

@implementation XGGroupModel

- (instancetype)initWithGroup:(EMGroup *)group
{
    if (self = [super init]) {
        _group = group;
    }
    
    return self;
}

+ (instancetype)groupModelWithGroup:(EMGroup *)group
{
    return [[self alloc] initWithGroup:group];
}

@end
