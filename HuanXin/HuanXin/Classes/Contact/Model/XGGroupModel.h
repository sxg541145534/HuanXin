//
//  XGGroupModel.h
//  HuanXin
//
//  Created by monkey on 2019/12/3.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EMGroup;

@interface XGGroupModel : NSObject

/// 群聊模型
@property (nonatomic,strong) EMGroup *group;
/// 事件
@property (nonatomic,copy) void(^selectedCallBlock)(NSString *title);
/// 是否可用
@property (nonatomic,assign,getter=isEnable) BOOL enable;
/// 是否已加入
@property (nonatomic,assign,getter=isJoined) BOOL joined;

- (instancetype)initWithGroup:(EMGroup *)group;
+ (instancetype)groupModelWithGroup:(EMGroup *)group;

@end
