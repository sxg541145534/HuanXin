//
//  XGUserModel.h
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGUserModel : NSObject

/// 用户名
@property (nonatomic,copy,readonly) NSString *userName;

- (instancetype)initWithUserName:(NSString *)userName;
+ (instancetype)userModelWithUserName:(NSString *)userName;

@end
