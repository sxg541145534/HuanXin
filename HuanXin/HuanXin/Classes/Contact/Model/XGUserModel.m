//
//  XGUserModel.m
//  HuanXin
//
//  Created by monkey on 2019/7/13.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGUserModel.h"

@implementation XGUserModel

- (instancetype)initWithUserName:(NSString *)userName
{
    if (self = [super init]) {
        _userName = [userName copy];
    }
    
    return self;
}

+ (instancetype)userModelWithUserName:(NSString *)userName
{
    return [[self alloc] initWithUserName:userName];
}

@end
